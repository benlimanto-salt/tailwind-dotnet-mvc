const colors = require('tailwindcss/colors')
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./Views/**/*.{cshtml,js}"
  ],
  theme: {
    colors: {
      danger: colors.red[600],
      ...colors
    },
    extend: {},
  },
  plugins: [
  ]
}

