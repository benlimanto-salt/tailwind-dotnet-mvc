﻿// Please see documentation at https://learn.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(function() {
    $(".mobile-menu-btn").click(function() {
        let expand = $("#mobile-menu").attr("expanded");
        if (expand == "true")
        {
            $("#mobile-menu").attr("expanded", "false").addClass("hidden");
        }
        else 
        {
            $("#mobile-menu").attr("expanded", "true").removeClass("hidden");
        }
    });
});