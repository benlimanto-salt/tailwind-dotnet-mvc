# Tailwind .NET Core

This is a playground for tailwind dotnet core 8.0 LTS

## How to run 

Use this command 
```
dotnet watch
# on other terminal run
npx tailwindcss -i ./wwwroot/css/tailwind.css -o ./wwwroot/css/main.css --watch
```

## What is the use

This is only a poc for myself regarding tailwind with .NET Core